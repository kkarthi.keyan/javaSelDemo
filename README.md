# Automated UI Tests
Automate UI tests written using java-selenium-maven-testNG

## Pre-requisites
    * Install and configure JDK 11
    * Install and configure Apache Maven 3.6.0+ 
    * Download and install the latest version of Eclipse with TestNG plugin
    * Clone the project using git and import to eclipse 
    * Install Docker to run the tests inside a container

## project structure

Project is structured as a standard Maven project:

    javaSelDemo
    |-- src/test/java        
    |-- src/test/resources
        |-- api
        |-- drivers
        |-- suites
    |-- src/main/java
    |-- src/main/resources
    |-- ConfigFile.xml - global test configuration
    |-- pom.xml
    |-- Dockerfile
    |-- run.sh
    |-- entrypoint.sh


## Running tests

There are several options to execute a test: 

To run test from Eclipse IDE, just select the test method: Right click > Run As > TestNG suite

To run test suite from Eclipse IDE, just select the required TestNG xml file: Right click > Run As > TestNG suite

To run test suite from the console, navigate to the test project root (where pom.xml is located) and execute the following command:

    $ mvn clean test -DBROWSER=<chrome or firefox>

To run test from the console, navigate to the test project root (where pom.xml is located) and execute the following command:

    $ mvn clean test -Dtest=<Test class>  -DBROWSER=<chrome or firefox>

## Implementation Details

1. Implemented thread safe webdriver facliltating parallel runs
2. Using log4j for logging the test events 
2. Generating extentreports 
3. Send email reports using jakarta mail (gmail)
4. Conternarized the tests using docker (Execute the run.sh in the root directory to run the tests in a docker container)
5. Screenshots on failure (Check target/screenshots directory on failure)
