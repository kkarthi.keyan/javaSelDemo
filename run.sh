#!/bin/bash

docker build -t chromeselenium . \
&& docker run -d -p 4444:4444 -p 5901:5900 --shm-size="2g" --name seleniumtests chromeselenium \
&& docker exec seleniumtests /home/seluser/entrypoint.sh \
&& docker stop seleniumtests \
&& docker rm seleniumtests \ 
&& docker rmi chromeselenium


