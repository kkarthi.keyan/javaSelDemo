package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigFile {
	static Properties configFile;
	
	public static Properties loadConfigFile() throws IOException {
		FileInputStream fileinputstream= new FileInputStream("./src/test/resources/config.properties");
		Properties properties=new Properties();
		properties.load(fileinputstream);
		return properties;
	}

	  public static String getConfigVariable(String var) throws Exception{
		  configFile = loadConfigFile();
		  return configFile.getProperty(var);
	  }

	  public static String getBrowserName() throws Exception{
		  String browser =  System.getProperty("BROWSER");
		  if (browser == null) {
			  browser = getConfigVariable("Url");
		  }
		  return browser;
	  }

	  public static String getDefaultUrl() throws Exception{
		  String url = System.getProperty("URL");
		  if (url == null) {
			  url = getConfigVariable("URL");
		  }
		  return url;
	  }

	 
	  
}
