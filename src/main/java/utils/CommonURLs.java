package utils;

import java.lang.invoke.MethodHandles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommonURLs {
	protected static final Logger LOGGER = LogManager.getLogger(MethodHandles.lookup().lookupClass());

	public static String HOME_URL ;
	
	static {
		try {
			HOME_URL = ConfigFile.getDefaultUrl();
		} catch (Throwable e) {
			LOGGER.error(" initialization HOMA_URL failed due to UnknownHostException" + e.getMessage());
		}
	}	

}