package utils;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestPageSetup {
	private static final Logger log = LogManager.getLogger();
	protected WebDriver driver = null;
	private static ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();
	DriverManager driverManager = new DriverManager();

	@BeforeClass(alwaysRun = true)
	public void testSuiteSetup() throws Exception {
		try {
			setWebDriver(driverManager.initilaizeDriver(ConfigFile.getBrowserName()));
		} catch (Throwable t) {
			throw new SkipException(t.getMessage());
		}
	}

	public static WebDriver getDriver() {
		return threadLocalDriver.get();
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {		
		driverManager.closeDriver();
	}

//	@AfterSuite(alwaysRun = true)
	public void emailReport() throws Exception{
		SendEmail.emailReport();
	}

	@BeforeMethod(alwaysRun = true)
	public void runTestSetup() throws Exception {
		if (DriverManager.isDriverLive(getDriver())) {
		} else {
			try {
				setWebDriver(driverManager.initilaizeDriver(ConfigFile.getBrowserName()));
			} catch (Throwable t) {
				log.error("Skipping tests. WebDriver is null" + t.getMessage());
				throw new SkipException(t.getMessage());
			}
		}
	}

	protected void setWebDriver(WebDriver driver) throws Exception {
		try {
			threadLocalDriver.set(driver);
		} catch (Throwable t) {
			throw new SkipException(t.getMessage());
		}
	}

}