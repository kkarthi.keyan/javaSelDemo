package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DriverManager {
	private static final Logger log = LogManager.getLogger();

	private WebDriver driver = null;
	public static String OS;
	String IP;

	/**
	 * Function to initialize webDriver/ local or remote.
	 * 
	 * @param Browser type of browser
	 * @return instance of driver
	 */
	public WebDriver initilaizeDriver(String browserName) {
		OS = System.getProperty("os.name").toLowerCase();
		try {
			if (browserName.equalsIgnoreCase("chrome")) {
				if (OS.toLowerCase().contains("windows")) {
					System.setProperty("webdriver.chrome.driver", CommonLocators.driverFileDir + "chromedriver.exe");
				} else if (OS.toLowerCase().contains("linux")) {
					System.setProperty("webdriver.chrome.driver", CommonLocators.linuxDriverFileDir + "chromedriver");
				}else {
					System.setProperty("webdriver.chrome.driver", CommonLocators.driverFileDir + "chromedriver");
				}
				driver = new ChromeDriver(setChromeOptions());
			}
			if (browserName.equalsIgnoreCase("firefox")) {
				System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
				if (OS.toLowerCase().contains("windows")) {
					System.setProperty("webdriver.gecko.driver", CommonLocators.driverFileDir + "geckodriver.exe");
				} else if (OS.toLowerCase().contains("linux")) {
					System.setProperty("webdriver.gecko.driver", CommonLocators.linuxDriverFileDir + "geckodriver");
				}else {
					System.setProperty("webdriver.gecko.driver", CommonLocators.driverFileDir + "geckodriver");
				}
				driver = new FirefoxDriver(setFirefoxOptions());
			}

			return driver;
		} catch (Throwable e) {
			log.error(e.getMessage());
			return null;
		}
	}

	// https://chromedriver.chromium.org/capabilities
	protected ChromeOptions setChromeOptions() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		return options;
	}

	protected FirefoxOptions setFirefoxOptions() {
		FirefoxOptions options = new FirefoxOptions();
		options.setAcceptInsecureCerts(true);
		return options;
	}

	/**
	 * The function to return webDriver
	 * 
	 * @return
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * The function to quit driver
	 */
	public void quitDriver() {
		try {
			getDriver().quit();
		} catch (Throwable t) {
			log.error(t.getMessage());
		}
	}

	/**
	 * Function to close the driver.
	 */
	public void closeDriver() {
		getDriver().close();
		if (!isDriverLive(getDriver())) {
			quitDriver();
		}
	}

	/**
	 * Function to check if driver still is alive
	 * 
	 * @param driver
	 * @return true/false
	 */
	public static boolean isDriverLive(WebDriver driver) {

		if (driver == null || driver.toString().contains("null")) {
			log.error("driver null");
			return false;
		}
		return true;
	}

}
