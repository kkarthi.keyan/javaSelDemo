package utils;

import java.io.File;
import java.io.IOException;

import static utils.FileUtil.copyFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class TakeScreenshot  extends TestPageSetup {

    private static final Logger log = LogManager.getLogger();
    private static String fileFormat = ".png";

	/**
	 * Function to take screen shot
	 * 
	 * @param fileNmae
	 * @throws IOException
	 */

    public static void takeScreenshot(String name) throws IOException {
		try {
			TakesScreenshot screenshot = (TakesScreenshot) getDriver();
			File srcFile = screenshot.getScreenshotAs(OutputType.FILE);
			File destFile = new File(CommonLocators.screenshotDir + name + fileFormat);
			copyFile(srcFile, destFile);
		} catch (Throwable t) {
			log.error(t.getMessage());
		}
    }

}
