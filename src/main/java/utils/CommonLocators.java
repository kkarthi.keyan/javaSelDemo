package utils;


import java.io.File;

public class CommonLocators{
	public static String currentDir = File.separator + System.getProperty("user.dir");
	public static String screenshotDir = currentDir +  File.separator +"target" + File.separator + "screenshots" + File.separator ;
	public static String driverFileDir =  currentDir + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "drivers" + File.separator;
	public static String linuxDriverFileDir =  driverFileDir + File.separator + "linux" + File.separator;
	public static String emailReport =  currentDir + File.separator + "target" + File.separator + "surefire-reports" + File.separator + "emailable-report.html";
}