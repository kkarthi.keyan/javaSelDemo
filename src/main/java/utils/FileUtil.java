package utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileUtil {

    private static final Logger log = LogManager.getLogger();
    private static List<String> list;

    public static List<String> readFile(String filePath) {

        File file = new File(filePath);
        try {
            list = FileUtils.readLines(file, "UTF-8");
        } catch (IOException e) {
            log.error(e);
        }
        return list;
    }

    public static void copyFile(File srcFile, File destFile) {
        try {
            FileUtils.copyFile(srcFile, destFile);
        } catch (IOException e) {
            log.error(e);
        }
    }
    
}
