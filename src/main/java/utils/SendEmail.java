package utils;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.mail.BodyPart;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

public class SendEmail {
	private static final Logger log = LogManager.getLogger();

	public static void emailReport() throws InterruptedException {

		String to = "kart.test33@gmail.com";
		String from = "kart.test33@gmail.com";
		final String username = "kart.test33@gmail.com";
		final String password = "rbfjhnzhybijvlhg";
		String host = "smtp.gmail.com";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new jakarta.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			log.info("Getting read to send email report");
			Thread.sleep(1000);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject("Automated test report ");
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText("Please find the attached testNG report");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(CommonLocators.emailReport)));
			messageBodyPart.setFileName(CommonLocators.emailReport);
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			log.info("Email Sent Successfully");
		} catch (MessagingException e) {
			log.error("Failed to send Email report");
			throw new RuntimeException(e);
		}
	}

}
