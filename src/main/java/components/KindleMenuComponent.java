package components;

import static org.openqa.selenium.support.PageFactory.initElements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import pages.ProductPage;

public class KindleMenuComponent extends BasePage {

	@FindBy(css = "ul.hmenu-visible")
	private WebElement kindleMenu;

	@FindBy(xpath = "//a[@class='hmenu-item'] [contains(text(),'Kindle Paperwhite')]")
	private WebElement kindlePapewhiteLink;
	
	public KindleMenuComponent(WebDriver driver) throws Exception{
		super(driver);
		initElements(driver, this);
		waitUntilElementIsVisible(kindleMenu);
	}

	public ProductPage clickKindlePapewhite() throws Exception {
		waitUntilElementIsVisible(kindlePapewhiteLink);
		click(kindlePapewhiteLink);
		return new ProductPage(getDriver());
	}

}
