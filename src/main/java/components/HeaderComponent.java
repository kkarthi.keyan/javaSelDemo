package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import pages.SignInPage;

import static org.openqa.selenium.support.PageFactory.initElements;

public class HeaderComponent extends BasePage {

	@FindBy(css = "#nav-main")
	private WebElement headerBar;
	
	@FindBy(css = "#nav-main .nav-left")
	private WebElement allMenu;

	@FindBy(css = "a#nav-logo-sprites")
	private WebElement logo;

	@FindBy(css = "a#nav-link-accountList")
	private WebElement signInLink;

	public HeaderComponent(WebDriver driver) throws Exception{
		super(driver);
		initElements(driver, this);
		clickLogoIfHeaderBarisAbsent();
		waitUntilElementIsVisible(headerBar);
	}

	public NavigationBarComponent getNavBar() throws Exception {
		waitUntilElementIsVisible(allMenu);
		click(allMenu);
		return new NavigationBarComponent(getDriver());
	}
	
	public HeaderComponent clickLogoIfHeaderBarisAbsent() {
		if (!isDisplayed(headerBar)) {
			waitUntilElementIsVisible(logo);
			click(logo);
		}
		return this;
	}

	public SignInPage gotoSignInPage() throws Exception {
		waitUntilElementIsVisible(signInLink);
		click(signInLink);
		return new SignInPage(getDriver());
	}

}
