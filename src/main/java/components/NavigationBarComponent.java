package components;

import static org.openqa.selenium.support.PageFactory.initElements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

public class NavigationBarComponent extends BasePage {

	@FindBy(css = "ul.hmenu-visible")
	private WebElement navMenu;

	@FindBy(xpath = ".//a[@class='hmenu-item']/div[contains(text(),'Kindle')]")
	private WebElement kindleMenuLink;
	
	public NavigationBarComponent(WebDriver driver) throws Exception{
		super(driver);
		initElements(driver, this);
		waitUntilElementIsVisible(navMenu);
	}

	public KindleMenuComponent openKindleMenu() throws Exception {
		waitUntilElementIsVisible(kindleMenuLink);
		click(kindleMenuLink);
		return new KindleMenuComponent(getDriver());
	}

}
