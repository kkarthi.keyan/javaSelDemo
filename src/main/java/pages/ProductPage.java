package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.openqa.selenium.support.PageFactory.initElements;

public class ProductPage extends BasePage{

	@FindBy(css = "#main-image-container")
	private WebElement imageContainer;

	@FindBy(css = "input#add-to-cart-button")
	private WebElement addToCart;

	@FindBy(css = "span[id='submit.buy-now'] #buy-now-button")
	private WebElement buyNow;

	public ProductPage(WebDriver driver) throws Exception {
		super(driver);
		initElements(driver, this);
		waitUntilElementIsVisible(imageContainer);

	}

	@SuppressWarnings("unchecked")
	public <T> T clickBuyNow() throws Exception {
		waitUntilElementIsVisible(buyNow);
		click(buyNow);
		if (getPageURL().contains("signin")) {
			return (T) (new SignInPage(getDriver()));
		}
		return (T) (new CheckOutPage(getDriver()));
	}


}
