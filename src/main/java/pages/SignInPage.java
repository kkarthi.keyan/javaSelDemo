package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.PageFactory.initElements;

public class SignInPage extends BasePage {

	@FindBy(css = "#ap_email")
	private WebElement userName;

	@FindBy(css = "#ap_password")
	private WebElement password;

	@FindBy(css = "input#continue")
	private WebElement continueSiginIn;

	@FindBy(css = "input#signInSubmit")
	private WebElement siginIn;

	@FindBy(css = "#createAccountSubmit")
	private WebElement createAccount;

	public SignInPage(WebDriver driver) throws Exception {
		super(driver);
		initElements(driver, this);
		waitUntilElementIsVisible(createAccount);
	}

	public Boolean isUserNameDisplayed() {
		return isDisplayed(userName);
	}

	public void enterUserName(String uname) {
		waitUntilElementIsVisible(userName);
		sendKeys(userName, uname);
	}

	public void clickContinueSiginIn() {
		waitUntilElementIsVisible(continueSiginIn);
		click(continueSiginIn);
	}

	public void enterPassword(String pword) {
		waitUntilElementIsVisible(password);
		sendKeys(password, pword);
	}

	public HomePage clickSiginIn() {
		waitUntilElementIsVisible(siginIn);
		click(siginIn);
		return new HomePage(getDriver());
	}

}
