package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.openqa.selenium.support.PageFactory.initElements;

import components.HeaderComponent;
import components.NavigationBarComponent;
import utils.CommonURLs;

public class HomePage extends BasePage {
	
	@FindBy(css = "form[action*='Captcha']")
	private WebElement captcha;

	@FindBy(css = "#desktop-banner")
	private WebElement banner;

    public HomePage(WebDriver driver) {
		super(driver);
		initElements(driver, this);
	}

    public  HomePage gotoHomePage() throws Exception {
    	System.out.println(CommonURLs.HOME_URL);
		driver.get(CommonURLs.HOME_URL);
		refreshPageIfCatchaIsPresent();
		waitUntilElementIsVisible(banner);
		return this;
    }

	public HeaderComponent getHeader() throws Exception  {
		return new HeaderComponent(getDriver());
	}

	public NavigationBarComponent getNavBar() throws Exception  {
		return getHeader().getNavBar();
	}

	public HomePage refreshPageIfCatchaIsPresent() {
		if (isDisplayed(captcha)) {
			getDriver().navigate().refresh();
		}
		return new HomePage(getDriver());
	}

}
