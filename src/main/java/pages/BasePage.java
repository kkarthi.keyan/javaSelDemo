package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.urlContains;

/**
 * Page Util Class
 */

public class BasePage {
	protected static final Logger log = LogManager.getLogger();

	protected WebDriver driver = null;
	protected WebDriverWait wait = null;

	public BasePage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 5);
	}

	/**
	 * Function returns instance of web driver.
	 * 
	 * @return instance of web driver
	 */
	protected WebDriver getDriver() {
		return this.driver;
	}

	/**
	 * Function to Click a element.
	 * 
	 * @param WebElement
	 * @return void
	 */
	protected void click(WebElement element) {
		element.click();
	}

	/**
	 * Function to Clear element text.
	 * 
	 * @param WebElement
	 * @throws Exception
	 */
	protected void clear(WebElement element) {
		element.clear();
	}

	/**
	 * Send text to element.
	 * 
	 * @param WebElement
	 * @param keys       character sequence to send to the element
	 * @throws Exception
	 */
	public void sendKeys(WebElement element, String keys) {
		try {
			clear(element);
			element.sendKeys(keys);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	/**
	 * Get the inner text of this element.
	 * 
	 * @param WebElement
	 * @return The innerText of this element.
	 */
	public String getText(WebElement element) {
		return element.getText();
	}

	/**
	 * Checks if the element is displayed?
	 * 
	 * @param WebElement
	 * @return true or false
	 */
	protected boolean isDisplayed(WebElement element){
		try {
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Wait until element is visible.
	 * 
	 * @param WebElement
	 */
	protected void waitUntilElementIsVisible(WebElement element) {
		wait.until(visibilityOf(element));
	}

	/**
	 * Wait until URL contains the url fraction.
	 * 
	 * @param fraction - the fraction of the url that the page should be on
	 * @return true when the URL contains the text
	 */
	protected Boolean waitUntilUrlContains(String fraction) {
		return wait.until(urlContains(fraction));
	}

	/**
	 * Get current page current URL.
	 * 
	 * @return The URL of the page currently loaded in the browser
	 */
	public String getPageURL() {
		return getDriver().getCurrentUrl();
	}

}
