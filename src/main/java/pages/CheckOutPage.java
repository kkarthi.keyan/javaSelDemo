package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.openqa.selenium.support.PageFactory.initElements;

public class CheckOutPage extends BasePage{
	
	@FindBy(css = "#bottomSubmitOrderButtonId-announce")
	private WebElement placeYourOrder;

	@FindBy(css = "input[name*='claimCodeApply']")
	private WebElement applyClaimCode;

	@FindBy(css = "#spc-order-summary")
	private WebElement orderSummary;

	
    public CheckOutPage(WebDriver driver) throws Exception {
		super(driver);
		initElements(driver, this);
		waitUntilElementIsVisible(applyClaimCode);
	}
    
    public Boolean isOrderSummaryDisplayed() {
    	return isDisplayed(orderSummary);
    }

}
