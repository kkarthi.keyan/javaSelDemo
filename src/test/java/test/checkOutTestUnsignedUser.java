package test;

import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

public class checkOutTestUnsignedUser extends BaseTest {
	
	@Test()
	public void testCheckoutKindlePapewhite() throws Exception {
		kindleMenuComponent = homePage.getHeader().getNavBar().openKindleMenu();
		productPage = kindleMenuComponent.clickKindlePapewhite();
		signInPage = productPage.clickBuyNow();
		
		assertTrue(signInPage.isUserNameDisplayed(), "unsigned user should be redirected to signin page ");
	}
	
}
