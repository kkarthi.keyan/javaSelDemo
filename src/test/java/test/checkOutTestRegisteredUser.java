package test;

import static org.testng.Assert.assertTrue;

import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.HomePage;

public class checkOutTestRegisteredUser extends BaseTest {
	
	
	@BeforeMethod(alwaysRun = true)
	public void testSetup() throws Exception {
		try {
			homePage = new HomePage(getDriver()).gotoHomePage();
			signIn();
		} catch (Throwable t) {
			log.error("Skipping Tests. home page not loaded .................... " + t.getMessage());
			throw new SkipException("Skipping Tests. " + t.getMessage() );
		}
	}

	@Test()
	public void testCheckoutKindlePapewhite() throws Exception {
		kindleMenuComponent = homePage.getHeader().getNavBar().openKindleMenu();
		productPage = kindleMenuComponent.clickKindlePapewhite();
		checkOutPage = productPage.clickBuyNow();
		
		assertTrue(checkOutPage.isOrderSummaryDisplayed(), "RegisteredUser should see the checkout page ");
	}

	
	
}
