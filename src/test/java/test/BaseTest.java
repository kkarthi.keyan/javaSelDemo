package test;

import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;

import utils.TestPageSetup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import components.HeaderComponent;
import components.KindleMenuComponent;
import components.NavigationBarComponent;
import data.TestData;
import pages.CheckOutPage;
import pages.HomePage;
import pages.ProductPage;
import pages.SignInPage;

public class BaseTest extends TestPageSetup {
	
	protected static final Logger log = LogManager.getLogger();

	protected HomePage homePage;
	protected ProductPage productPage;
	protected SignInPage signInPage;
	protected CheckOutPage checkOutPage;
	protected HeaderComponent headerComponent;
	protected NavigationBarComponent navigationBarComponent;
	protected KindleMenuComponent kindleMenuComponent;
	
	@BeforeMethod(alwaysRun = true)
	public void testSetup() throws Exception {
		try {
			homePage = new HomePage(getDriver()).gotoHomePage();
		} catch (Throwable t) {
			log.error("Skipping Tests. home page not loaded .................... " + t.getMessage());
			throw new SkipException("Skipping Tests. " + t.getMessage() );
		}
	}

	public void signIn() throws Exception {	
		signInPage = homePage.getHeader().gotoSignInPage();
		signInPage.enterUserName(TestData.amazonUsername);
		signInPage.clickContinueSiginIn();
		signInPage.enterPassword(TestData.amazonPassword);
		signInPage.clickSiginIn();
	}
}
