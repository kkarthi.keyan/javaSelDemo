package listeners;

import static utils.TakeScreenshot.takeScreenshot;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    private static final Logger log = LogManager.getLogger();
//https://javadoc.io/doc/org.testng/testng/latest/org/testng/ITestListener.html
    @Override
    public void onStart(ITestContext context) {
        log.info("Suite test started: " + context.getName());
    }

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test method started: " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log.info("Test method passed: " + result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.warn("Test method skipped: " + result.getName());
        try {
			takeScreenshot(result.getName());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.error("Test method failed: " + result.getName());
        try {
			takeScreenshot(result.getName());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        log.warn("Test method failed but still within success percentage: " + result.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        log.info("Suite test finished: " + context.getName());
    }

}
