package listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {

    private static final Logger log = LogManager.getLogger();
//https://javadoc.io/doc/org.testng/testng/latest/org/testng/ISuiteListener.html
    @Override
    public void onStart(ISuite suite) {
        log.info("Test suite started: " + suite.getName());
    }

    @Override
    public void onFinish(ISuite suite) {
        log.info("Test suite finished: " + suite.getName());
    }

}
