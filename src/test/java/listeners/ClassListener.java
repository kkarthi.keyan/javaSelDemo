package listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.IClassListener;
import org.testng.ITestClass;

public class ClassListener implements IClassListener {

	//https://www.javadoc.io/doc/org.testng/testng/6.11/org/testng/IClassListener.html
    private static final Logger log = LogManager.getLogger();

    @Override
    public void onBeforeClass(ITestClass testClass) {
        log.info("Test class started: " + testClass.getName().substring(5));
    }

    @Override
    public void onAfterClass(ITestClass testClass) {
        log.info("Test class finished: " + testClass.getName().substring(5));
    }

}
