FROM selenium/standalone-chrome-debug:3.141.59

#============================================
#Installing git
#============================================
#RUN sudo apt-get update && sudo apt-get install -y git

#============================================
#Updating jdk
#============================================

RUN sudo apt-get update && sudo apt-get install -y openjdk-11-jdk

#============================================
#Installing maven
#============================================

RUN sudo apt-get update && sudo apt-get install -y maven

WORKDIR /home/seluser

#============================================
#Cloning test projects
#============================================

COPY ./ .

EXPOSE 4444
